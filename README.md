# Npmania IntelliJ IDEA Plugin


Npmania provides NPM (Node.JS Package Manager) utilities running from project view context menu IntelliJ UI brought for developer's
convenience especially when batch running on multiple and various packages is needed. The list is mostly based on package.json scan showing
tasks from scripts section that are met form all selected modules. For instance you want to publish all selected packages or run tests
for them, in this case you can just select modules and choose task from context menu. With locally installed npm repository software 
like Artifactory or Nexus you can have even kinda hot reload setup with a number of packages imported as IDE's modules and one stand package 
with their deployment. Just specify stand module name in "Reinstall" setting and it will update selected dependencies on every run.


# Usage Notes

Settings that you specified in Advanced Run dialog will be saved and used by default for actions run from
menu, e.g. Local repository or Ignore Failures

For Windows you may need to choose cmd.exe or something unix-compatible in Terminal configuration Settings section or
choose Ignore Fails option for command separators to be compatible with PowerShell

You can repeat last action with ctrl+alt+R hotkey

