             package net.techminded.npmania;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsActions;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.file.PsiDirectoryImpl;
import net.techminded.npmania.dto.PackageJson;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class NpmaniaHistoryActionGroup extends ActionGroup {

  private static final Logger logger = Logger.getInstance(NpmaniaHistoryActionGroup.class);

  public NpmaniaHistoryActionGroup(@Nullable @NlsActions.ActionText String label) {
    super(label, true);
  }

  @Override
  public void update(AnActionEvent event) {
    Project project = event.getProject();
    event.getPresentation().setEnabledAndVisible(project != null);
  }

  @Override
  public AnAction @NotNull [] getChildren(@Nullable AnActionEvent event) {
    Project project = event.getProject();
    NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
    List<AnAction> actions = new ArrayList<AnAction>();
    for (String actionCmd: settings.runHistory) {
      actions.add(new NpmaniaAction(actionCmd.length() > 100 ? actionCmd.substring(0, 100) + ".." : actionCmd, actionCmd));
    }
    Collections.reverse(actions);
    return actions.toArray(new AnAction[] {});
  }
}