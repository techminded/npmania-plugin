package net.techminded.npmania;


import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsActions;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class NpmaniaRepeatAction extends NpmaniaAction {

    private static final Logger logger = Logger.getInstance(NpmaniaRepeatAction.class);

    public NpmaniaRepeatAction() {
        super("");
    }

    public NpmaniaRepeatAction(@Nullable @NlsActions.ActionText String text) {
        super(text);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        String command = settings.runHistory.size() > 0 ? settings.runHistory.peek() : null;
        if (command != null) {
            runCommand(project, project.getBasePath(), command);
        } else {
            logger.warn("Attempt to run command from empty history");
        }
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        e.getPresentation().setEnabledAndVisible(true);
    }
}
