package net.techminded.npmania;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

@State(
      name = "net.techminded.npmania.NpmaniaSettingsState"
)
public class NpmaniaProjectSettingsState implements PersistentStateComponent<NpmaniaProjectSettingsState> {

  public String localRepoUrl = "http://localhost:8081/repository/loc";
  public String npmCommand = "npm";
  public boolean ignoreFails = false;
  public Map<String, String> actions = new HashMap<>();
  public Map<String, Map<String, String>> moduleScripts = new HashMap<>();
  public Map<String, String> mandatorActions = new HashMap<>() {{
    put("npm publish", getState().npmCommand + " publish");
    put("npm publish --registry=" + getState().localRepoUrl, getState().npmCommand + " publish --registry=" + getState().localRepoUrl);
    put("Clean reinstall node modules", "rm ./node_modules/ -rf && rm ./package-lock.json && " + getState().npmCommand + "i");
  }};
  // Update this modules after tasks has been running
  public List<String> autoUpdateModules = new ArrayList<>();
  public Integer runHistorySize = 20;
  public Stack<String> runHistory = new Stack<>();

  @Nullable
  @Override
  public NpmaniaProjectSettingsState getState() {
    return this;
  }

  @Override
  public void loadState(@NotNull NpmaniaProjectSettingsState state) {
    XmlSerializerUtil.copyBean(state, this);
  }

}