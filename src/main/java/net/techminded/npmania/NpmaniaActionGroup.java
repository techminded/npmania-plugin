package net.techminded.npmania;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.file.PsiDirectoryImpl;
import net.techminded.npmania.dto.PackageJson;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.intellij.openapi.diagnostic.Logger;

public class NpmaniaActionGroup extends ActionGroup implements UpdateInBackground {

  private static final Logger logger = Logger.getInstance(NpmaniaActionGroup.class);

  @Override
  public void update(AnActionEvent event) {
    Project project = event.getProject();
    event.getPresentation().setEnabledAndVisible(project != null);
  }

  private Map<String, String> updateActions(Map<String, String> npmActions, Map<String, String> moduleScripts) {
    // add new actions
    for (String scriptName : moduleScripts.keySet()) {
      if (!npmActions.containsKey(scriptName)) {
        npmActions.put(scriptName, moduleScripts.get(scriptName));
      }
    }
    // keep only non-unique actions
    npmActions = npmActions.entrySet().stream().filter((npmAction) ->
                    moduleScripts.keySet().contains(npmAction.getKey()))
            .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    return npmActions;
  }

  @Override
  public AnAction @NotNull [] getChildren(@Nullable AnActionEvent event) {
    Project project = event.getProject();
    NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
    List<AnAction> actions = new ArrayList<AnAction>() {{
      add(new NpmaniaRunDialogAction("Configure/Advanced Run"));
      add(new NpmaniaHistoryActionGroup("Run History"));
      add(new NpmaniaRepeatAction("Repeat previous Run"));
      add(new NpmaniaFlushModuleScriptsAction("Flush scripts cache"));
    }};
    for (String actionKey: settings.mandatorActions.keySet()) {
      actions.add(new NpmaniaAction(actionKey, settings.mandatorActions.get(actionKey)));
    }

    ObjectMapper mapper = new ObjectMapper();
    Map<String, String> npmActions = new HashMap<>();
    Object @Nullable [] items = event.getDataContext().getData(PlatformDataKeys.SELECTED_ITEMS);
    if (items != null) {
      for (Object item : items) {
        PsiDirectoryImpl directory = null;
        try {
          directory = (PsiDirectoryImpl) item;
        } catch (Exception e) {
          continue;
        }
        if (! settings.getState().moduleScripts.containsKey(directory.getName())) { // not found in module cache
          for (PsiFile file : directory.getFiles()) { // read module files
            if ("package.json".equals(file.getName())) {
              try {
                PackageJson packageJson = mapper.readValue(new File(file.getVirtualFile().getPath()), PackageJson.class);
                if (packageJson.getScripts() != null) {
                  updateActions(npmActions, packageJson.getScripts());
                }
              } catch (IOException ex) {
                logger.warn("Error reading module's " + directory.getName() + " package.json file");
              }
              break;
            }
          }
        } else { // load tasks from cached data
          updateActions(npmActions, settings.getState().moduleScripts.get(directory.getName()));
        }
      }
      // add new actions scanned from package.json
      if (npmActions.keySet().size() > 0) {
        settings.getState().actions = new HashMap<>();
        for (String action : npmActions.keySet()) {
          actions.add(new NpmaniaAction("npm run-script " + action,settings.getState().npmCommand + " run-script " + action));
          settings.getState().actions.put("npm run-script " + action, settings.getState().npmCommand + " run-script " + action);
        }
      }
    }
    return actions.toArray(new AnAction[] {});
  }
}