package net.techminded.npmania;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTextField;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.FormBuilder;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.Arrays;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class NpmaniaRunDialogWrapper extends DialogWrapper {

    private Project project;
    private JPanel mainPanel;
    private JCheckBox ignoreFailsCheckbox = new JCheckBox();
    private JBTextField localRepoUrlTextField = new JBTextField();
    private JBTextField npmCommandTextField = new JBTextField();
    private JBTextField updateModulesTextField = new JBTextField();
    private JButton upButton = new JButton("Up");
    private JButton downButton = new JButton("Down");
    JPanel tableActionButtons = new JPanel(new FlowLayout(FlowLayout.LEFT));
    {
        tableActionButtons.add(upButton);
        tableActionButtons.add(downButton);
        upButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] rows = actionsTable.getSelectedRows();
                if (rows.length > 0) {
                    int firstRow = rows[0];
                    if (firstRow >= 1) {
                        for (int row : rows) {
                            Object selectedValue = actionsTable.getModel().getValueAt(row, 0);
                            Object upperValue = actionsTable.getModel().getValueAt(row - 1, 0);
                            actionsTable.getModel().setValueAt(selectedValue, row - 1, 0);
                            actionsTable.getModel().setValueAt(upperValue, row, 0);
                        }
                        actionsTable.getSelectionModel().setSelectionInterval(
                                actionsTable.getSelectionModel().getMinSelectionIndex() - 1,
                                actionsTable.getSelectionModel().getMaxSelectionIndex() - 1
                        );
                    }
                }
            }
        });
        downButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] rows = actionsTable.getSelectedRows();
                if (rows.length > 0) {
                    int firstRow = rows[0];
                    if (firstRow  < actionsTable.getRowCount() - 1) {
                        for (int row : Arrays.reverse(rows)) {
                            Object selectedValue = actionsTable.getModel().getValueAt(row, 0);
                            Object nextValue = actionsTable.getModel().getValueAt(row + 1, 0);
                            actionsTable.getModel().setValueAt(selectedValue, row + 1, 0);
                            actionsTable.getModel().setValueAt(nextValue, row, 0);
                        }
                        actionsTable.getSelectionModel().setSelectionInterval(
                                actionsTable.getSelectionModel().getMinSelectionIndex() + 1,
                                actionsTable.getSelectionModel().getMaxSelectionIndex() + 1
                        );
                    }
                }
            }
        });
    }
    private JBTable actionsTable;
    private Map<String, String> actions;

    public NpmaniaRunDialogWrapper(Map<String, String> actions, Project project) {
        super(true);
        setTitle("Run NPM Scripts");
        if (actions != null) {
            this.actions = actions;
        }
        if (project != null) {
            this.project = project;
        }
        init();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        List<Object[]> dataList = new LinkedList<>();
        for (String actionLabel : actions.keySet()) {
            dataList.add(new Object[] { actionLabel });
        }
        actionsTable = new JBTable(new DefaultTableModel(dataList.toArray(new Object[0][]), new Object[] {"Task Selection"})) {
            public boolean isCellEditable(int row, int column) {
                return false;
            };
        };
        actionsTable.getTableHeader().setReorderingAllowed(true);
        actionsTable.setRowSelectionAllowed(true);

        actionsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        actionsTable.getColumnModel().getColumn(0).setPreferredWidth(350);

        JBScrollPane tablePane = new JBScrollPane(actionsTable, JBScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JBScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        mainPanel = FormBuilder.createFormBuilder()
            .addLabeledComponent(new JBLabel("Ignore fails: "), ignoreFailsCheckbox, 1, false)
            .addLabeledComponent(new JBLabel("Local repo URL: "), localRepoUrlTextField, 1, false)
            .addLabeledComponent(new JBLabel("Npm command: "), npmCommandTextField, 1, false)
            .addLabeledComponent(new JBLabel("Actions: "), tablePane, 1, false)
            .addLabeledComponent(new JBLabel(""), tableActionButtons, 1, false)
            .addLabeledComponent(new JBLabel("Reinstall in modules (comma-sep. list): "), updateModulesTextField, 1, false)
            .addComponentFillVertically(new JPanel(), 0)
            .getPanel();
        return mainPanel;
    }

    @Nullable
    @Override
    protected ValidationInfo doValidate() {
        if (actionsTable.getSelectedColumnCount() <= 0) {
            return new ValidationInfo("One or more tasks must be selected", actionsTable);
        }
        if (! StringUtils.isBlank(updateModulesTextField.getText())) {
            String updateCSListMessage = "Submodules to update must be set at comma-separated list of names";
            try {
                String[] updateModules = updateModulesTextField.getText().split(",");
                if (updateModules.length <= 0) {
                    return new ValidationInfo(updateCSListMessage, updateModulesTextField);
                } else {
                    for (String moduleName : updateModules) {
                        String pathString = project.getBasePath() + "/" + moduleName + "/package.json";
                        Path path = Paths.get(pathString);
                        if (! Files.exists(path)) {
                            return new ValidationInfo(moduleName + " has no package.json", updateModulesTextField);
                        }
                    }
                }
            } catch (Exception e) {
                return new ValidationInfo(updateCSListMessage, updateModulesTextField);
            }
        }
        return null;
    }

    public JCheckBox getIgnoreFailsCheckbox() {
        return ignoreFailsCheckbox;
    }

    public void setIgnoreFailsCheckbox(JCheckBox ignoreFailsCheckbox) {
        this.ignoreFailsCheckbox = ignoreFailsCheckbox;
    }

    public JBTextField getLocalRepoUrlTextField() {
        return localRepoUrlTextField;
    }

    public void setLocalRepoUrlTextField(JBTextField localRepoUrlTextField) {
        this.localRepoUrlTextField = localRepoUrlTextField;
    }

    public JBTextField getNpmCommandTextField() {
        return npmCommandTextField;
    }

    public void setNpmCommandTextField(JBTextField npmCommandTextField) {
        this.npmCommandTextField = npmCommandTextField;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JBTable getActionsTable() {
        return actionsTable;
    }

    public void setActionsTable(JBTable actionsTable) {
        this.actionsTable = actionsTable;
    }

    public JBTextField getUpdateModulesTextField() {
        return updateModulesTextField;
    }

    public void setUpdateModulesTextField(JBTextField updateModulesTextField) {
        this.updateModulesTextField = updateModulesTextField;
    }
}