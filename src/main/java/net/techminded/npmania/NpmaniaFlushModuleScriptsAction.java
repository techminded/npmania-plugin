package net.techminded.npmania;


import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsActions;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

public class NpmaniaFlushModuleScriptsAction extends NpmaniaAction {

    private static final Logger logger = Logger.getInstance(NpmaniaFlushModuleScriptsAction.class);

    public NpmaniaFlushModuleScriptsAction() {
        super("");
    }

    public NpmaniaFlushModuleScriptsAction(@Nullable @NlsActions.ActionText String text) {
        super(text);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        settings.getState().actions = new HashMap<>();
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        e.getPresentation().setEnabledAndVisible(true);
    }
}
