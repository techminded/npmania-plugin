package net.techminded.npmania;


import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsActions;
import com.intellij.psi.impl.file.PsiDirectoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NpmaniaRunDialogAction extends NpmaniaAction {

    private static final Logger logger = Logger.getInstance(NpmaniaRunDialogAction.class);

    public NpmaniaRunDialogAction(@Nullable @NlsActions.ActionText String text) {
        super(text);
    }

    private void runCommands(NpmaniaRunDialogWrapper runDialog, NpmaniaProjectSettingsState settings, Map<String, String> actions, AnActionEvent e) {
        Project project = e.getProject();
        settings.ignoreFails = runDialog.getIgnoreFailsCheckbox().isSelected();
        settings.localRepoUrl = runDialog.getLocalRepoUrlTextField().getText();
        if (! settings.npmCommand.equals(runDialog.getNpmCommandTextField().getText())) {
            settings.npmCommand = runDialog.getNpmCommandTextField().getText();
            settings.getState().actions = new HashMap<>();
        }
        settings.autoUpdateModules = (! "".equals(runDialog.getUpdateModulesTextField().getText())) ?
                Arrays.asList(runDialog.getUpdateModulesTextField().getText().split(",")) : new ArrayList<>();

        int[] selectedRows = runDialog.getActionsTable().getSelectedRows();
        List<String> tasks = new ArrayList<String>();
        for (int rowIndex : selectedRows) {
            String task = (String) runDialog.getActionsTable().getValueAt(rowIndex, 0);
            tasks.add(actions.get(task));
        }
        Object @Nullable [] items = e.getDataContext().getData(PlatformDataKeys.SELECTED_ITEMS);
        List<String> commands = new ArrayList<String>();
        List<String> updateNames = new ArrayList<String>();
        if (items != null) {
            for (Object item : items) {
                PsiDirectoryImpl directory = (PsiDirectoryImpl) item;
                commands.add("cd " + directory.getVirtualFile().getPath());
                commands.add(String.join(settings.getState().getState().ignoreFails ? " ; " : " && ", tasks));
                commands.add("cd " + project.getBasePath());

                updateNames.add(directory.getName());
            }
            addUpdateCommands(project, settings, commands, updateNames);
            String command = String.join(settings.getState().getState().ignoreFails ? " ; " : " && ", commands);
            runCommand(project, project.getBasePath(), command);
            addToHistory(command, project);
        }

    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        Map<String, String> actions = new HashMap<>();
        for (String actionKey: settings.mandatorActions.keySet()) {
            actions.put(actionKey, settings.mandatorActions.get(actionKey));
        }
        for (String actionKey: settings.actions.keySet()) {
            actions.put(actionKey, settings.actions.get(actionKey));
        }
        NpmaniaRunDialogWrapper runDialog = new NpmaniaRunDialogWrapper(actions, project);
        runDialog.getIgnoreFailsCheckbox().setSelected(settings.ignoreFails);
        runDialog.getLocalRepoUrlTextField().setText(settings.localRepoUrl);
        runDialog.getNpmCommandTextField().setText(settings.npmCommand);
        runDialog.getUpdateModulesTextField().setText(String.join(",", settings.autoUpdateModules));
        runDialog.getActionsTable().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table = (JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int row = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
                    runDialog.close(0);
                    runCommands(runDialog, settings, actions, e);
                }
            }
        });
        if (runDialog.showAndGet()) {
            if (runDialog.getActionsTable().getSelectedColumnCount() > 0) {
                runCommands(runDialog, settings, actions, e);
            }
        }
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        e.getPresentation().setEnabledAndVisible(project != null);
    }
}
