package net.techminded.npmania;


import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsActions;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.impl.file.PsiDirectoryImpl;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.terminal.ShellTerminalWidget;
import org.jetbrains.plugins.terminal.TerminalToolWindowFactory;
import org.jetbrains.plugins.terminal.TerminalView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class NpmaniaAction extends AnAction {

    private static final Logger logger = Logger.getInstance(NpmaniaAction.class);

    private String label;
    private String cmd;

    private ShellTerminalWidget terminalWidget;

    public NpmaniaAction(@Nullable @NlsActions.ActionText String text) {
        super(text);
        cmd = text;
    }

    public NpmaniaAction(@Nullable @NlsActions.ActionText String label, String cmd) {
        super(label);
        this.label = label;
        this.cmd = cmd;
    }

    protected void runCommand(Project project, String path, String command) {
        TerminalView terminalView = TerminalView.getInstance(project);
        ToolWindow window = ToolWindowManager.getInstance(project).getToolWindow(TerminalToolWindowFactory.TOOL_WINDOW_ID);
        if (window == null) return;
        try {
            if (terminalWidget == null) {
                terminalWidget = terminalView.createLocalShellWidget(path, "Npmania");
            }
            terminalWidget.executeCommand(command);
        } catch (IOException e) {
            logger.error("Error running command " + command + " at the path " + path);
        }
    }

    public void cleanHistory(Project project) {
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        if (settings.runHistory.size() > settings.runHistorySize) {
            Stack<String> newStack = new Stack<>();
            int i = 1;
            for (String historyCommand : settings.runHistory) {
                if (i >= (settings.runHistorySize - 1)) {
                    break;
                }
                newStack.push(historyCommand);
                i++;
            }
            settings.runHistory = newStack;
        }
    }

    public void addToHistory(String command, Project project) {
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        String prevCommand = settings.runHistory.size() > 0 ? settings.runHistory.peek() : null;
        if (! command.equals(prevCommand)) {
            settings.runHistory.push(command);
        }
        cleanHistory(project);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        NpmaniaProjectSettingsState settings = project.getService(NpmaniaProjectSettingsState.class);
        Object @Nullable [] items = e.getDataContext().getData(PlatformDataKeys.SELECTED_ITEMS);
        List<String> commands = new ArrayList();
        List<String> updateNames = new ArrayList<String>();
        if (items != null) {
            for (Object item : items) {
                PsiDirectoryImpl directory = (PsiDirectoryImpl) item;
                commands.add("cd " + directory.getVirtualFile().getPath());
                commands.add(cmd);
                commands.add("cd " + project.getBasePath());
                updateNames.add(directory.getName());
            }
            addUpdateCommands(project, settings, commands, updateNames);
            String command = String.join(settings.getState().getState().ignoreFails ? " ; " : " && ", commands);
            runCommand(project, project.getBasePath(), command);
            addToHistory(command, project);

        }
    }

    protected void addUpdateCommands(Project project, NpmaniaProjectSettingsState settings, List<String> commands, List<String> updateNames) {
        for (String updModuleName : settings.autoUpdateModules) {
            if (! StringUtils.isBlank(updModuleName)) {
                commands.add("cd " + project.getBasePath() + "/" + updModuleName);
                commands.add(settings.npmCommand + " i " + String.join(" ", updateNames));
                commands.add("cd " + project.getBasePath());
            }
        }
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        Project project = e.getProject();
        e.getPresentation().setEnabledAndVisible(project != null);
    }
}
